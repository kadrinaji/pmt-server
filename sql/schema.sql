-- Users

CREATE TABLE users (
    email varchar(120),
    firstName varchar(30),
    lastName varchar(30),
	active BOOLEAN DEFAULT 0,
	sessionID  varchar(120),
    sessionKey varchar(120),
    verifyHash text NOT NULL,
    PRIMARY KEY (email)
)

-- Activation

CREATE TABLE activation (
    email varchar(120),
    token text NOT NULL,
    PRIMARY KEY (email),
    FOREIGN KEY (email) REFERENCES users(email)
    ON DELETE CASCADE ON UPDATE CASCADE
)

-- Category

CREATE TABLE category (
    email varchar(120),
    name varchar(60) NOT NULL,
    PRIMARY KEY (email, name),
    FOREIGN KEY (email) REFERENCES users(email)
    ON DELETE CASCADE ON UPDATE CASCADE
)

-- Passwords

CREATE TABLE passwords (
    email varchar(120),
    category varchar(60),
    url varchar(256),
    username varchar(60),
    salt text,
    pass text,
    notes text,
    PRIMARY KEY (email, url, username),
    FOREIGN KEY (email) REFERENCES users(email)
    ON DELETE CASCADE ON UPDATE CASCADE,
    FOREIGN KEY (email, category) REFERENCES category(email, name)
    ON DELETE CASCADE ON UPDATE CASCADE
)