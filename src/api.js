const express = require('express');
const apiRouter = express.Router();
const gensalt = require('@kdf/salt');
const nodemailer = require('nodemailer');
const { google } = require("googleapis");
const OAuth2 = google.auth.OAuth2;
const verificationEmail = require('./verification-email');
const dbm = require('./database-manager').instance(); // create an instance of our database manager

// format datetime object to match that of MYSQL
// this would be put in its own module and imported
// but since it's not a software engineering course
// and this is the only route we will have
// we put this function here instead
function formatDateTime(date_ob) {

    let date = ("0" + date_ob.getDate()).slice(-2);

    // current month
    let month = ("0" + (date_ob.getMonth() + 1)).slice(-2);

    // current year
    let year = date_ob.getFullYear();

    // current hours
    let hours = date_ob.getHours();

    // current minutes
    let minutes = date_ob.getMinutes();

    // current seconds
    let seconds = date_ob.getSeconds();

    return year + "-" + month + "-" + date + " " + hours + ":" + minutes + ":" + seconds;
}

function sendVerificationEmail(email, token) {

    const { CLIENT_ID, CLIENT_SECRET, CLIENT_EMAIL, REFRESH_TOKEN } = process.env;

    const createTransporter = async () => {

        const oauth2Client = new OAuth2(
            CLIENT_ID,
            CLIENT_SECRET,
            "https://developers.google.com/oauthplayground"
        );

        oauth2Client.setCredentials({
            refresh_token: REFRESH_TOKEN
        });

        const accessToken = await new Promise((resolve, reject) => {
            oauth2Client.getAccessToken((err, token) => {
                if (err) {
                    // reject("Failed to create access token :(");
                    reject(err);
                }
                resolve(token);
            });
        }).catch((err) => {
            console.log(err);
            return process.env.ACCESS_TOKEN;
        })

        const transporter = nodemailer.createTransport({
            service: "gmail",
            auth: {
                type: "OAuth2",
                user: CLIENT_EMAIL,
                accessToken,
                clientId: CLIENT_ID,
                clientSecret: CLIENT_SECRET,
                refreshToken: REFRESH_TOKEN
            },
            tls: {
                rejectUnauthorized: false
            }
        });

        return transporter;

    }

    const sendEmail = async (emailOptions) => {
        let emailTransporter = await createTransporter();
        await emailTransporter.sendMail(emailOptions);
    };

    const mailOptions = {
        from: CLIENT_EMAIL,
        to: email,
        subject: 'Password Management Tool - Email Verification',
        html: verificationEmail(token)
    };

    sendEmail(mailOptions);
}

// create a default JSON response for API endpoints
// success property shows if the intended action/query was successful
function defaultResponse(message = null, success = true) {

    let response = {};

    response.success = success;

    if (message) {
        if (success) {
            response.message = message;
        } else {
            response.error = message;
        }
    }

    return response;
}

// a response that contains JSON data, always success: true
function dataResponse(data, message = null) {

    let response = {}

    response.success = true;

    if (message) {
        response.message = message;
    }

    response.data = data;

    return response;
}

apiRouter.get('/test', (_, res) => {
    res.send('<h3>Password Management Tool API server is running...</h3>');
})

apiRouter.get('/dbm/test', (_, res) => {
    dbm.executeQuery('SELECT 1 + 1', () => {
        res.send('<h3>Database is connected...</h3>');
    })
})

apiRouter.post('/register', (req, res) => {
    const data = req.body;

    const valid = data.email && data.firstName && data.lastName && data.verifyHash;



    if (valid) {

        dbm.prepared(dbm.queries.getUser, [data.email], (rows) => {
            if (rows.length == 0) {
                dbm.prepared(dbm.queries.registerUser,
                    [data.email, data.firstName, data.lastName, data.verifyHash], () => {
                        dbm.prepared(dbm.queries.initUncategorized, [data.email], () => {
                            gensalt(16).then(hash => {
                                const activationToken = hash.toString('hex');
                                let date = new Date();
                                date.setMinutes(date.getMinutes() + 30);
                                const expiration = formatDateTime(date);
                                dbm.prepared(dbm.queries.createVerifyToken, [data.email, activationToken, expiration], () => {
                                    sendVerificationEmail(data.email, activationToken);
                                    res.json(defaultResponse('user successfully registered'));
                                })
                            })
                        })
                    })
            } else {
                res.json(defaultResponse('user already registered', false))
            }
        })

    } else {
        res.status(400).json(defaultResponse('must add to request body: email, firstName, lastName, verifyHash', false))
    }
})

apiRouter.post('/login', (req, res) => {

    const data = req.body;

    const valid = data.email && data.verifyHash;

    if (valid) {
        dbm.prepared(dbm.queries.getUser, [data.email], (rows) => {
            if (rows.length == 1) {
                const user = rows[0];

                if (user.active == 0) {
                    return res.status(403).json(defaultResponse('user not verified. verify email before login', false));
                }

                if (data.verifyHash == user.verifyHash) {
                    if (req.session.email) {
                        res.json(defaultResponse('user already logged in', false));
                    } else {
                        gensalt(16).then(hash => {
                            const sessionKey = hash.toString('base64');
                            const sessionID = req.session.id;
                            dbm.prepared(dbm.queries.storeSession, [sessionID, sessionKey, user.email], () => {
                                req.session.email = user.email;
                                let response = defaultResponse('user logged in successfully');
                                response.sessionKey = sessionKey;
                                response.firstName = user.firstName;
                                response.lastName = user.lastName;
                                res.json(response);
                            })
                        })
                    }
                } else {
                    res.status(400).json(defaultResponse('wrong credentials', false));
                }
            } else {
                res.status(400).json(defaultResponse('user not registered', false));
            }

        })
    } else {
        res.status(400).json(defaultResponse('must add to request body: email, verifyHash', false))
    }
})

apiRouter.post('/logout', (req, res) => {
    req.session.destroy();
    res.json(defaultResponse('current session logged out'));
})


apiRouter.get('/user', (req, res) => {
    if(req.session.email){
        dbm.prepared(dbm.queries.getUser, [req.session.email], (rows) => {
            const user = rows[0];
            let response = defaultResponse();
            response.data = {
                email: user.email,
                firstName: user.firstName,
                lastName: user.lastName,
            }
            return res.json(response);
        })
    }else{
        res.sendStatus(401);
    }
})

// get list of categories
apiRouter.get('/user/category', (req, res) => {
    if (req.session.email) {
        dbm.prepared(dbm.queries.getCategories, [req.session.email], (rows) => {
            let response = defaultResponse();
            response.data = rows.map(category => category.name);
            res.json(response);
        })
    } else {
        res.sendStatus(401);
    }
})

// create categories
apiRouter.post('/user/category', (req, res) => {
    if (req.session.email) {

        const data = req.body;

        const valid = data.name;

        if (valid) {
            dbm.prepared(dbm.queries.addCategory, [req.session.email, data.name], (_, err) => {
                if (err) {
                    res.json(defaultResponse('category already exists', false));
                } else {
                    res.json(defaultResponse('category added successfully'));
                }
            })
        } else {
            res.status(400).json(defaultResponse('must add to request body: name', false))
        }
    } else {
        res.sendStatus(401);
    }
})

apiRouter.put('/user/category', (req, res) => {
    if (req.session.email) {

        const data = req.body;

        const valid = data.name && data.update && data.update.name;

        if (valid) {
            dbm.prepared(dbm.queries.updateCategory, [data.update.name, req.session.email, data.name], (rows, err) => {
                if (err) {
                    res.sendStatus(500);
                } else {
                    if (rows.affectedRows > 0) {
                        res.json(defaultResponse('category updated successfully'));
                    } else {
                        res.status(404).json(defaultResponse('category does not exist', false));
                    }
                }
            })
        } else {
            res.status(400).json(defaultResponse('must add to request body: name, update.name', false))
        }
    } else {
        res.sendStatus(401);
    }
})


apiRouter.delete('/user/category', (req, res) => {
    if (req.session.email) {
        const data = req.body;
        const valid = data.name;

        if (valid) {
            dbm.prepared(dbm.queries.deleteCategory, [req.session.email, data.name], (rows) => {
                if (rows.affectedRows > 0) {
                    res.json(defaultResponse('category removed from vault'));
                } else {
                    res.status(404).json(defaultResponse('category does not exist', false));
                }
            })
        } else {
            res.status(400).json(defaultResponse('must add to request body: name', false));
        }

    } else {
        res.sendStatus(401);
    }
})

// store a new password or update an existing one
apiRouter.post('/user/password', (req, res) => {
    if (req.session.email) {

        let data = req.body;

        const valid = data.category && data.url && data.username && data.pass;

        data.salt = data.salt || '';
        data.notes = data.notes || '';

        const query_data = [req.session.email, data.category, data.url, data.username, data.salt, data.pass, data.notes]

        if (valid) {
            dbm.prepared(dbm.queries.addCategory, [req.session.email, data.category], () => {
                dbm.prepared(dbm.queries.addPassword, query_data,
                    (_, err) => {
                        if (err) {
                            if (err.code == 'ER_DUP_ENTRY') {
                                res.json(defaultResponse('password already exits in vault', false));
                            } else {
                                res.sendStatus(500);
                            }
                        } else {
                            res.json(defaultResponse('password added to vault'));
                        }
                    });
            });
        } else {
            res.status(400).json(defaultResponse('must add to request body: category, url, username, [salt], pass, [notes]', false))
        }

    } else {
        res.sendStatus(401);
    }
})

// get all passwords or one password if there is a query
apiRouter.get('/user/password', (req, res) => {
    if (req.session.email) {
        if (req.query.url && req.query.username) {
            const url = req.query.url;
            const username = req.query.username;

            dbm.prepared(dbm.queries.getPassword, [req.session.email, url, username], (rows) => {
                if (rows.length > 0) {
                    let response = defaultResponse();
                    response = defaultResponse();
                    response.data = rows[0];
                    res.json(response);
                } else {
                    res.status(404).json(defaultResponse('password does not exist', false));
                }
            })

        } else {
            dbm.prepared(dbm.queries.getPasswords, [req.session.email], (rows) => {
                let response = defaultResponse();
                response.data = rows.map(item => {
                    return {
                        category: item.category,
                        username: item.username,
                        url: item.url,
                        salt: item.salt,
                        pass: item.pass,
                        notes: item.notes
                    }
                })

                res.json(response);
            })
        }
    } else {
        res.sendStatus(401);
    }
})

// update existing password
apiRouter.put('/user/password', (req, res) => {
    if (req.session.email) {
        const data = req.body;

        const valid = data.url && data.username;

        if (valid) {

            dbm.prepared(dbm.queries.getPassword, [req.session.email, data.url, data.username], (rows) => {
                if (rows.length > 0) {
                    const oldPass = rows[0];

                    if (!data.update) {
                        data.update = {};
                    }

                    const url = data.update.url || oldPass.url;
                    const username = data.update.username || oldPass.username;
                    const salt = data.update.salt || oldPass.salt;
                    const pass = data.update.pass || oldPass.pass;
                    const category = data.update.category || oldPass.category;
                    const notes = data.update.notes || oldPass.notes;

                    const query_data = [category, url, username, salt, pass, notes, req.session.email, data.url, data.username]

                    dbm.prepared(dbm.queries.addCategory, [req.session.email, category], () => {
                        dbm.prepared(dbm.queries.updatePassword, query_data, () => {
                            res.json(defaultResponse('password updated successfully'));
                        })
                    })

                } else {
                    res.status(404).json(defaultResponse('password does not exist', false));
                }
            })
        } else {
            res.status(400).json(defaultResponse('must add to request body: url, username, [update]', false));
        }

    } else {
        res.sendStatus(401);
    }
})


apiRouter.delete('/user/password', (req, res) => {
    if (req.session.email) {
        const data = req.query;
        const valid = data.url && data.username;

        if (valid) {
            dbm.prepared(dbm.queries.deletePassword, [req.session.email, data.url, data.username], (rows) => {
                if (rows.affectedRows > 0) {
                    res.json(defaultResponse('password removed from vault'));
                } else {
                    res.status(404).json(defaultResponse('password does not exist', false));
                }
            })
        } else {
            res.status(400).json(defaultResponse('must add to request body: url, username', false));
        }

    } else {
        res.sendStatus(401);
    }
})


// get the session key to encrypt/decrypt master key on local device
apiRouter.get('/user/session/key', (req, res) => {
    if (req.session.email) {
        dbm.prepared(dbm.queries.getSessionKey, [req.session.email, req.session.id], (rows) => {
            if (rows.length == 1) {
                let session = rows[0];
                let response = defaultResponse('session key granted');
                response.data = {
                    "sessionKey": session.sessionKey
                }
                res.json(response);
            } else {
                res.status(403).json(defaultResponse('wrong session ID, cannot grant session key', false));
            }
        })
    } else {
        res.sendStatus(401);
    }
})

// email verification using a token
apiRouter.get('/verify/token/:token', (req, res) => {
    dbm.prepared(dbm.queries.getValidToken, [req.params.token], (rows) => {
        if (rows.length == 1) {
            const user = rows[0];
            dbm.prepared(dbm.queries.verifyAccount, [user.email], () => {
                dbm.prepared(dbm.queries.consumeToken, [req.params.token], () => {
                    if (req.query.from == 'email') {
                        res.redirect('../success');
                    } else {
                        res.json(defaultResponse('account successfully verified'));
                    }
                })
            })
        } else {
            res.status(404).json(defaultResponse('token expired or does not exist', false));
        }
    })
});

apiRouter.get('/verify/success', (req, res) => {
    res.send('<h1 style="text-align: center">account successfully verified!</h1>')
})

module.exports = apiRouter;