const express = require('express');
const cookieParser = require('cookie-parser');
const session = require('express-session');
const morgan = require('morgan');
const apiRouter = require('./api');
const cors = require('cors');

require('dotenv').config()

const port = 3000;
const app = express();

const whitelist = new Set(["chrome-extension://mebfleiokpaacbokoibeokdifjbbeima", "http://127.0.0.1:3000", "http://localhost:3000", "http://127.0.0.1:5500", "http://localhost:5500"]);

const corsOptions = {
    credentials: true,
    optionsSuccessStatus: 200,
    origin: function(origin, callback){
        console.log("origin: " + origin);
        if(whitelist.has(origin) || origin == undefined){
            callback(null, true);
        }else{
            callback(new Error('Not allowed by CORS'));
        }
    }
}


app.use(express.urlencoded({ extended: false }));
app.use(express.json());

app.options('*', cors(corsOptions));
app.use(cors(corsOptions));

// app.use(function (req, res, next) {
//     res.header('Content-Type', 'application/json;charset=UTF-8')
//     res.header('Access-Control-Allow-Credentials', true)
//     res.header(
//         'Access-Control-Allow-Headers',
//         'Origin, X-Requested-With, Content-Type, Accept'
//     )
//     res.header('access-control-expose-headers', 'Set-Cookie');
//     next()
// })

app.use(cookieParser('cmps283-pmt-server'));
app.use(session({
    secret: 'cmps283-pmt-server',
    resave: false,
    saveUninitialized: false,
    proxy: true,
    cookie: {
        maxAge: 1000 * 60 * 10, // 10 minutes
        sameSite: 'none',
        secure: true
    }
}));

app.use(morgan('dev'));

app.use('/api', apiRouter);

app.listen(port, () => {console.log(`server running at port ${port}`)});

