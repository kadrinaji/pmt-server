module.exports = (token) => 
`
<h1>Password Management Tool</h1>
<p>Hello,</p>
<p>Thank you for signing up!</p>
<p>Click on the activation link to start using the Password Management Tool:</p>
<a href="${process.env.BASE_URL || 'http://localhost:3000' }/api/verify/token/${token}?from=email">Activate Now </a>
`;