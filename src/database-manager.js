const env = process.env.NODE_ENV || 'development';
const mysql2 = require('mysql2');

class DatabaseManager {

    constructor() {

        if(!!DatabaseManager.instance){
            return DatabaseManager.instance;
        }

        DatabaseManager.instance = this;

        const { DB_HOST } = process.env;
        const { DB_USER } = process.env;
        const { DB_PASS } = process.env;
        const { DB_DATABASE } = process.env;

        if (env == 'development'){

            this.settings = {
                host: 'localhost',
                port: 3308,
                user: 'root',
                database: 'passwordmanager'
            }
            
        }else{

            this.settings = {
                host: DB_HOST,
                user: DB_USER,
                password: DB_PASS,
                database: DB_DATABASE,
            }
        }

        this.connection = this.setupConnection();

        // make sure to connect and refresh the connection to keep it alive
        this.connection.connect( (err) => {

            if(err){
                throw err;
            }

            setInterval( () => {
                this.connection.query('SELECT 1 + 1', [], (err) => {
                    // console.log('Error: Cannot Connect to Database!');
                });
            }, 20000);

        });

        return this;
    }  

    queries = {
        getUser: () => "SELECT * FROM users WHERE email = ?",
        registerUser: () => "INSERT INTO users (email, firstName, lastName, verifyHash) VALUES(?, ?, ?, ?)",
        initUncategorized: () => "REPLACE INTO category VALUES(?, 'uncategorized')",
        storeSession: () => "UPDATE users SET sessionID = ?, sessionKey = ? WHERE email = ?",
        getCategories: () => "SELECT *  FROM category WHERE email = ?",
        addCategory: () => "INSERT IGNORE category VALUES(?, ?)",
        updateCategory: () => "UPDATE category SET name = ? WHERE email = ? AND name = ?",
        deleteCategory: () => "DELETE FROM category WHERE email = ? AND name = ?",
        getPasswords: () => "SELECT * FROM passwords WHERE email = ?",
        getPassword: () => "SELECT * FROM Passwords WHERE email = ? AND url = ? AND username = ?",
        addPassword: () => "INSERT INTO passwords VALUES(?, ?, ?, ?, ?, ?, ?)",
        updatePassword: () => "UPDATE passwords SET category = ?, url = ?, username= ?, salt = ?, pass = ?, notes = ?  WHERE email = ? AND url = ? AND username = ?",
        deletePassword: () => "DELETE FROM passwords WHERE email = ? AND url = ? AND username = ?",
        getSessionKey: () => "SELECT sessionID, sessionKey FROM users WHERE email = ? AND sessionID = ?",
        createVerifyToken: () => "REPLACE INTO activation VALUES(?, ?, ?)",
        getValidToken: () => "SELECT * FROM activation WHERE token = ? AND expiration > SYSDATE()",
        verifyAccount: () => "UPDATE users SET active = 1 WHERE email = ?",
        consumeToken: () => "DELETE FROM activation WHERE token = ?"
    }

}

// return an open connection to the database; need to close manually
DatabaseManager.prototype.setupConnection = function () {
    return mysql2.createConnection(this.settings);
}


// used for regular queries
DatabaseManager.prototype.query = function (query, resultFunc) {

    // get the string value if the query is passed as a function 
    if(typeof query == 'function'){
        query = query();
    }

    // the callback function should be err, rows, fields but we don't use fields meta-data
    this.connection.query( query, (err, rows, _ )=>{

        resultFunc(rows, err)
        
        if(err){
            console.error(err);
        }

    });

}

// used for prepared statements
DatabaseManager.prototype.prepared = function (query, data,  resultFunc) {

    // get the string value if the query is passed as a function 
    if(typeof query == 'function'){
        query = query();
    }

    // the callback function should be err, rows, fields but we don't use fields meta-data
    this.connection.execute( query, data,  (err, rows, _ )=>{

        resultFunc(rows, err);

        if(err){
            console.error(err);
        }

    });

}

module.exports = { instance : function() { return new DatabaseManager() } };
